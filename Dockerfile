ARG BASE
FROM ${BASE}

RUN apt-get update -y && apt-get install -y git squashfs-tools wget xxd bsdmainutils binutils

RUN git clone https://github.com/woachk/houdini
RUN cd houdini && ./download.sh && ./build.sh && ./install64.sh
RUN rm -rf houdini

ADD run.sh .
RUN chmod +x run.sh
CMD ./run.sh
